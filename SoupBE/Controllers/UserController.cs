﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using SoupBE.Models;

namespace PaymentApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UserController : ControllerBase
    {
        private readonly IConfiguration _configuration;

        public UserController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        // HTTP GET: /User
        [HttpGet]
        public ActionResult<List<User>> GetUsers()
        {
            List<User> users = LoadUsersFromDB();
            return users;
        }

        // HTTP POST: /User
        [HttpPost]
        public IActionResult AddUser(User user)
        {
            InsertUserToDB(user);
            return Ok("User added successfully.");
        }

        // HTTP PUT: /User/{id}
        [HttpPut("{id}")]
        public IActionResult UpdateUser(int id, User updatedUser)
        {
            bool isUpdated = UpdateUserInDB(id, updatedUser);
            if (isUpdated)
            {
                return Ok("User updated successfully.");
            }
            else
            {
                return NotFound("User not found.");
            }
        }

        // HTTP PUT: /User/SetActiveStatus/{id}
        [HttpPut("SetActiveStatus/{id}")]
        public IActionResult SetActiveStatus(int id, bool isActive)
        {
            bool isUpdated = UpdateUserStatusInDB(id, isActive);
            if (isUpdated)
            {
                return Ok("User status updated successfully.");
            }
            else
            {
                return NotFound("User not found.");
            }
        }

        private List<User> LoadUsersFromDB()
        {
            List<User> users = new List<User>();
            string query = "SELECT * FROM [User]";
            using (SqlConnection conn = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(query, conn);
                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    User user = new User
                    {
                        Id = Convert.ToInt32(reader["id"]),
                        Username = reader["username"].ToString(),
                        Email = reader["email"].ToString(),
                        Password = reader["password"].ToString(),
                        FullName = reader["fullname"].ToString(),
                        Birthdate = reader["birthdate"] != DBNull.Value ? Convert.ToDateTime(reader["birthdate"]) : (DateTime?)null,
                        Address = reader["address"].ToString(),
                        Phone = reader["phone"].ToString(),
                        IsActive = Convert.ToBoolean(reader["IsActive"]),
                    };

                    users.Add(user);
                }
            }

            return users;
        }

        private void InsertUserToDB(User user)
        {
            string query = "INSERT INTO [User] (id, username, email, password, fullname, birthdate, address, phone, IsActive) " +
                           "VALUES (@id, @username, @email, @password, @fullName, @birthdate, @address, @phone, @isActive)";
            using (SqlConnection conn = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.Parameters.AddWithValue("@id", user.Id);
                cmd.Parameters.AddWithValue("@username", user.Username);
                cmd.Parameters.AddWithValue("@email", user.Email);
                cmd.Parameters.AddWithValue("@password", user.Password);
                cmd.Parameters.AddWithValue("@fullName", user.FullName);
                cmd.Parameters.AddWithValue("@birthdate", user.Birthdate != null ? (object)user.Birthdate : DBNull.Value);
                cmd.Parameters.AddWithValue("@address", user.Address);
                cmd.Parameters.AddWithValue("@phone", user.Phone);
                cmd.Parameters.AddWithValue("@isActive", user.IsActive);
                cmd.ExecuteNonQuery();
            }
        }

        private bool UpdateUserInDB(int id, User updatedUser)
        {
            string query = "UPDATE [User] SET username = @username, email = @email, " +
                           "password = @password, fullname = @fullName, birthdate = @birthdate, " +
                           "address = @address, phone = @phone WHERE id = @id";
            using (SqlConnection conn = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.Parameters.AddWithValue("@username", updatedUser.Username);
                cmd.Parameters.AddWithValue("@email", updatedUser.Email);
                cmd.Parameters.AddWithValue("@password", updatedUser.Password);
                cmd.Parameters.AddWithValue("@fullName", updatedUser.FullName);
                cmd.Parameters.AddWithValue("@birthdate", updatedUser.Birthdate != null ? (object)updatedUser.Birthdate : DBNull.Value);
                cmd.Parameters.AddWithValue("@address", updatedUser.Address);
                cmd.Parameters.AddWithValue("@phone", updatedUser.Phone);
                int rowsAffected = cmd.ExecuteNonQuery();
                return rowsAffected > 0;
            }
        }

        private bool UpdateUserStatusInDB(int id, bool isActive)
        {
            string query = "UPDATE [User] SET IsActive = @isActive WHERE id = @id";
            using (SqlConnection conn = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.Parameters.AddWithValue("@isActive", isActive);
                int rowsAffected = cmd.ExecuteNonQuery();
                return rowsAffected > 0;
            }
        }
    }
}
