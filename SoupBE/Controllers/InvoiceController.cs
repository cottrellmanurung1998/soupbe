﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using SoupBE.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace InvoiceApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class InvoicesController : ControllerBase
    {
        private readonly IConfiguration _connection;

        public InvoicesController(IConfiguration connection)
        {
            _connection = connection;
        }

        // Request Get Invoices
        [HttpGet]
        public List<Invoice> GetInvoices()
        {
            List<Invoice> outputList = new List<Invoice>();
            outputList = LoadInvoiceListFromDB();
            return outputList;
        }

        // Request Get Invoice By Id
        [HttpGet("{id}")]
        public ActionResult<Invoice> GetInvoiceByID(int id)
        {
            Invoice invoice = LoadInvoiceFromDB(id);
            if (invoice == null)
            {
                return NotFound();
            }

            return invoice;
        }

        // Request Get Invoice Detail by InvoiceId
        [HttpGet("{id}/details")]
        public ActionResult<List<InvoiceDetail>> GetInvoiceDetail(int id)
        {
            List<InvoiceDetail> outputList = new List<InvoiceDetail>();
            outputList = LoadInvoiceDetailListFromDB(id);
            return outputList;
        }

        private List<Invoice> LoadInvoiceListFromDB()
        {
            List<Invoice> outputList = new List<Invoice>();
            string query = "SELECT * FROM Invoice";
            using (SqlConnection conn = new SqlConnection(_connection.GetConnectionString("DefaultConnection")))
            {
                SqlCommand cmd = new SqlCommand(query, conn);
                SqlDataAdapter dataAdapter = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                dataAdapter.Fill(dt);

                foreach (DataRow row in dt.Rows)
                {
                    Invoice iModel = new Invoice
                    {
                        InvoiceId = Convert.ToInt32(row["invoice_id"]),
                        InvoiceNumber = row["invoice_number"].ToString(),
                        InvoiceDate = Convert.ToDateTime(row["invoice_date"]),
                        TotalPrice = row["total_price"].ToString(),
                    };

                    outputList.Add(iModel);
                }
            }

            return outputList;
        }

        private Invoice LoadInvoiceFromDB(int id)
        {
            string query = "SELECT * FROM Invoice WHERE invoice_id = @id";
            using (SqlConnection conn = new SqlConnection(_connection.GetConnectionString("DefaultConnection")))
            {
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.Parameters.AddWithValue("@id", id);
                conn.Open();
                SqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    reader.Read();
                    Invoice iModel = new Invoice
                    {
                        InvoiceId = Convert.ToInt32(reader["invoice_id"]),
                        InvoiceNumber = reader["invoice_number"].ToString(),
                        InvoiceDate = Convert.ToDateTime(reader["invoice_date"]),
                        TotalPrice = reader["total_price"].ToString(),
                    };

                    return iModel;
                }
            }

            return null;
        }

        private List<InvoiceDetail> LoadInvoiceDetailListFromDB(int invoiceId)
        {
            List<InvoiceDetail> outputList = new List<InvoiceDetail>();
            string query = "SELECT * FROM InvoiceDetail WHERE invoice_id = @id";
            using (SqlConnection conn = new SqlConnection(_connection.GetConnectionString("DefaultConnection")))
            {
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.Parameters.AddWithValue("@id", invoiceId);
                SqlDataAdapter dataAdapter = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                dataAdapter.Fill(dt);

                foreach (DataRow row in dt.Rows)
                {
                    InvoiceDetail detail = new InvoiceDetail
                    {
                        DetailId = Convert.ToInt32(row["detail_id"]),
                        No = Convert.ToInt32(row["no"]),
                        CourseName = row["course_name"].ToString(),
                        Type = row["type"].ToString(),
                        Schedule = row["schedule"].ToString(),
                        Price = row["price"].ToString(),
                        InvoiceId = Convert.ToInt32(row["invoice_id"]),
                    };

                    outputList.Add(detail);
                }
            }

            return outputList;
        }
    }
}
