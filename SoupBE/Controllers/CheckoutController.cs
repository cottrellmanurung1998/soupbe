﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using SoupBE.Models;

namespace PaymentApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CheckoutController : ControllerBase
    {
        private readonly IConfiguration _configuration;

        public CheckoutController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        // HTTP GET: /Checkout
        [HttpGet]
        public ActionResult<List<Checkout>> GetCheckoutItems()
        {
            List<Checkout> checkoutItems = LoadCheckoutItemsFromDB();
            return checkoutItems;
        }

        // HTTP POST: /Checkout
        [HttpPost]
        public IActionResult AddCheckoutItem(Checkout checkoutItem)
        {
            InsertCheckoutItemToDB(checkoutItem);
            return Ok("Checkout item added successfully.");
        }

        // HTTP DELETE: /Checkout/{id}
        [HttpDelete("{id}")]
        public IActionResult DeleteCheckoutItem(int id)
        {
            bool isDeleted = DeleteCheckoutItemFromDB(id);
            if (isDeleted)
            {
                return Ok("Checkout item deleted successfully.");
            }
            else
            {
                return NotFound("Checkout item not found.");
            }
        }

        private List<Checkout> LoadCheckoutItemsFromDB()
        {
            List<Checkout> checkoutItems = new List<Checkout>();
            string query = "SELECT * FROM Checkout";
            using (SqlConnection conn = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(query, conn);
                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    Checkout checkoutItem = new Checkout
                    {
                        Id = Convert.ToInt32(reader["id"]),
                        Picture = reader["picture"].ToString(),
                        Type = reader["type"].ToString(),
                        Course = reader["course"].ToString(),
                        Schedule = reader["schedule"].ToString(),
                        Price = reader["price"].ToString(),
                    };

                    checkoutItems.Add(checkoutItem);
                }
            }

            return checkoutItems;
        }

        private void InsertCheckoutItemToDB(Checkout checkoutItem)
        {
            string query = "INSERT INTO Checkout (id, picture, type, course, schedule, price) VALUES (@id, @picture, @type, @course, @schedule, @price)";
            using (SqlConnection conn = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.Parameters.AddWithValue("@id", checkoutItem.Id);
                cmd.Parameters.AddWithValue("@picture", checkoutItem.Picture);
                cmd.Parameters.AddWithValue("@type", checkoutItem.Type);
                cmd.Parameters.AddWithValue("@course", checkoutItem.Course);
                cmd.Parameters.AddWithValue("@schedule", checkoutItem.Schedule);
                cmd.Parameters.AddWithValue("@price", checkoutItem.Price);
                cmd.ExecuteNonQuery();
            }
        }

        private bool DeleteCheckoutItemFromDB(int id)
        {
            string query = "DELETE FROM Checkout WHERE id = @id";
            using (SqlConnection conn = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.Parameters.AddWithValue("@id", id);
                int rowsAffected = cmd.ExecuteNonQuery();
                return rowsAffected > 0;
            }
        }
    }
}
