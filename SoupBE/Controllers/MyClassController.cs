﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using SoupBE.Models;

namespace MyClassApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MyClassController : ControllerBase
    {
        private readonly IConfiguration _connection;

        public MyClassController(IConfiguration connection)
        {
            _connection = connection;
        }

        // GET: /MyClass
        [HttpGet]
        public List<MyClass> GetMyClass()
        {
            List<MyClass> outputList = new List<MyClass>();
            string query = "SELECT * FROM MyClass";

            using (SqlConnection conn = new SqlConnection(_connection.GetConnectionString("DefaultConnection")))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(query, conn);
                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    MyClass myClass = new MyClass
                    {
                        Id = Convert.ToInt32(reader["Id"]),
                        Picture = reader["Picture"].ToString(),
                        Type = reader["Type"].ToString(),
                        CourseName = reader["CourseName"].ToString(),
                        Schedule = reader["Schedule"].ToString(),
                    };

                    outputList.Add(myClass);
                }
            }

            return outputList;
        }
    }
}
