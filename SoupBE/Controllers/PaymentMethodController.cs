﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using SoupBE.Models;

namespace PaymentApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PaymentMethodController : ControllerBase
    {
        private readonly IConfiguration _configuration;

        public PaymentMethodController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        // HTTP GET: /PaymentMethod
        [HttpGet]
        public ActionResult<List<PaymentMethod>> GetPaymentMethods()
        {
            List<PaymentMethod> paymentMethods = LoadPaymentMethodsFromDB();
            return paymentMethods;
        }

        // HTTP POST: /PaymentMethod
        [HttpPost]
        public IActionResult AddPaymentMethod(PaymentMethod paymentMethod)
        {
            InsertPaymentMethodToDB(paymentMethod);
            return Ok("Payment method added successfully.");
        }

        // HTTP PUT: /PaymentMethod/{id}
        [HttpPut("{id}")]
        public IActionResult UpdatePaymentMethod(int id, PaymentMethod updatedPaymentMethod)
        {
            bool isUpdated = UpdatePaymentMethodInDB(id, updatedPaymentMethod);
            if (isUpdated)
            {
                return Ok("Payment method updated successfully.");
            }
            else
            {
                return NotFound("Payment method not found.");
            }
        }

        // HTTP PUT: /PaymentMethod/SetActiveStatus/{id}
        [HttpPut("SetActiveStatus/{id}")]
        public IActionResult SetActiveStatus(int id, bool isActive)
        {
            bool isUpdated = UpdatePaymentMethodStatusInDB(id, isActive);
            if (isUpdated)
            {
                return Ok("Payment method status updated successfully.");
            }
            else
            {
                return NotFound("Payment method not found.");
            }
        }

        private List<PaymentMethod> LoadPaymentMethodsFromDB()
        {
            List<PaymentMethod> paymentMethods = new List<PaymentMethod>();
            string query = "SELECT * FROM PaymentMethod";
            using (SqlConnection conn = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(query, conn);
                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    PaymentMethod paymentMethod = new PaymentMethod
                    {
                        Id = Convert.ToInt32(reader["id"]),
                        Picture = reader["picture"].ToString(),
                        Pay = reader["pay"].ToString(),
                        IsActive = Convert.ToBoolean(reader["IsActive"]),
                    };

                    paymentMethods.Add(paymentMethod);
                }
            }

            return paymentMethods;
        }

        private void InsertPaymentMethodToDB(PaymentMethod paymentMethod)
        {
            string query = "INSERT INTO PaymentMethod (id, picture, pay, IsActive) VALUES (@id, @picture, @pay, @isActive)";
            using (SqlConnection conn = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.Parameters.AddWithValue("@id", paymentMethod.Id);
                cmd.Parameters.AddWithValue("@picture", paymentMethod.Picture);
                cmd.Parameters.AddWithValue("@pay", paymentMethod.Pay);
                cmd.Parameters.AddWithValue("@isActive", paymentMethod.IsActive);
                cmd.ExecuteNonQuery();
            }
        }

        private bool UpdatePaymentMethodInDB(int id, PaymentMethod updatedPaymentMethod)
        {
            string query = "UPDATE PaymentMethod SET picture = @picture, pay = @pay WHERE id = @id";
            using (SqlConnection conn = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.Parameters.AddWithValue("@picture", updatedPaymentMethod.Picture);
                cmd.Parameters.AddWithValue("@pay", updatedPaymentMethod.Pay);
                int rowsAffected = cmd.ExecuteNonQuery();
                return rowsAffected > 0;
            }
        }

        private bool UpdatePaymentMethodStatusInDB(int id, bool isActive)
        {
            string query = "UPDATE PaymentMethod SET IsActive = @isActive WHERE id = @id";
            using (SqlConnection conn = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.Parameters.AddWithValue("@isActive", isActive);
                int rowsAffected = cmd.ExecuteNonQuery();
                return rowsAffected > 0;
            }
        }
    }
}
