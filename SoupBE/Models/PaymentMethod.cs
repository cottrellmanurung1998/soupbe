﻿namespace SoupBE.Models
{
    public class PaymentMethod
    {
        public int Id { get; set; }
        public string Picture { get; set; }
        public string Pay { get; set; }
        public bool IsActive { get; set; }
    }

}
