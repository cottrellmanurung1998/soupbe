﻿namespace SoupBE.Models
{
    // Checkout.cs (Model)
    public class Checkout
    {
        public int Id { get; set; }
        public string Picture { get; set; }
        public string Type { get; set; }
        public string Course { get; set; }
        public string Schedule { get; set; }
        public string Price { get; set; }
    }

}
