﻿namespace SoupBE.Models
{
    public class MyClass
    {
        public int Id { get; set; }
        public string Picture { get; set; }    
        public string Type { get; set; }
        public string CourseName { get; set; }
        public string Schedule { get; set; }
    }
}