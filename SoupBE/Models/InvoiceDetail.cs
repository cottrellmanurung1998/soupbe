﻿namespace SoupBE.Models
{
    public class InvoiceDetail
    {
        public int DetailId { get; set; }
        public int No { get; set; }
        public string CourseName { get; set; }
        public string Type { get; set; }
        public string Schedule { get; set; }
        public string Price { get; set; }

        public int InvoiceId { get; set; }
    }
}
